import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  param = {
    world: { value: 'world tnexnguyen' }
  };
  constructor(translate: TranslateService) {
    // this language will be used as a fallback when a translation isn't found in the current language
    translate.setDefaultLang('en');
    translate.setTranslation('en', {
      HELLO: 'hello {{value}}'
    });
    translate.get('HELLO', { value: 'world' }).subscribe((res: string) => {
      console.log(res);
      //=> 'hello world'
    });
    // the lang to use, if the lang isn't available, it will use the current loader to get them
    translate.use('en');
  }
  title = 'app works!';
}
