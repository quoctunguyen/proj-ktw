import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PageComponent } from './route/page/page.component';
import { PostComponent } from './route/post/post.component';
import { SharedModule } from './shared/shared.module';
import { AppService } from 'app/app.service';

import { LayoutsModule } from './layouts/layouts.module';
//lazy loading
import { MainComponent } from './route/main/main.component';
const appRoutes: Routes = [
  {
    path: '',
    loadChildren: './route/main/main.module#MainModule', 
  },
  // {
  //   path: 'news',
  //   loadChildren: './route/page/page.module#PageModule',
  //   // redirectTo: '/heroes',
  // },
  {
    path: 'post',
    // outlet: 'content',
    component: PostComponent,
    // outlet: 'content',
  },

  // { path: '**', component: PageNotFoundComponent }
];
 


@NgModule({
  declarations: [
    AppComponent,
    PostComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpModule,
    SharedModule,
    LayoutsModule,
    TranslateModule.forRoot(),
    RouterModule.forRoot(appRoutes)],
  providers: [
    AppService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
