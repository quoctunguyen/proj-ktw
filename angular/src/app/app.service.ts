import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
@Injectable()
export class AppService {

  private activeLink = new BehaviorSubject<object>({});
  constructor() { }

  setActiveLink(res) {
    this.activeLink.next(res);
  }

  onSidebarToggle() {
    return this.activeLink;
  }

}
