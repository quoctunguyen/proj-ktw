import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { NavbarComponent } from './navbar/navbar.component';
import { SharedModule } from '../shared/shared.module';
import { SidebarComponent } from './sidebar/sidebar.component';
import { MenuComponent } from './menu/menu.component';
@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    SharedModule
  ],
  declarations: [
    NavbarComponent,
    SidebarComponent,
    MenuComponent,
  ],
  exports: [
    NavbarComponent,
    SharedModule,
    SidebarComponent,
    MenuComponent 
  ]
})
export class LayoutsModule { }
 