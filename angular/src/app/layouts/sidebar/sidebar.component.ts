import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import * as _ from 'lodash';
import { Observable, BehaviorSubject } from 'rxjs';
import { AppService } from '../../../app/app.service';
declare let $: any;
declare let SmoothlyMenu: any;
@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']

})
export class SidebarComponent implements OnInit {

  public hero = {
    state: 'active'
  }
  constructor(public _router: Router, private _appService: AppService) {
    this._appService
      .onSidebarToggle()
      // .take(1)
      .subscribe((res: any) => {
        this.classActive = res;
        console.log(this.classActive);
      })
  }

  public classActive = {
    active: '',
    sub: ''
  };
  public menu = [
    {
      // Bộ dụng cụ
      path: '/',
      name: 'Siêu thị',
      fa: 'fa fa-th-large',
      childs: [
      ]
    },
    {
      path: '/page',
      name: 'Device',
      fa: 'fa fa-table',
      childs: [
        {
          path: '/page/create',
          name: 'New Page',
          childs: []
        },
        {
          path: '/page',
          name: 'List',
          childs: []
        },
        {
          path: '',
          name: 'Category or page',
          childs: []
        }
      ]
    },
    {
      path: '/slide',
      name: 'Sensor',
      fa: 'fa fa-asterisk',
      childs: [
        {
          path: '/product/create',
          name: 'New slide',
          childs: []
        },
        {
          path: '/page',
          name: 'List',
          childs: []
        }
      ]
    },
    {
      path: '/slide',
      name: 'Zone',
      fa: 'fa fa-globe',
      childs: [
        {
          path: '/product/create',
          name: 'New slide',
          childs: []
        },
        {
          path: '/page',
          name: 'List',
          childs: []
        }
      ]
    },
    {
      path: '/product',
      name: 'User',
      fa: 'fa fa-user-circle-o',
      childs: [
        {
          path: '/user/create',
          name: 'New user',
          childs: []
        }, 
        {
          path: '/user',
          name: 'List',
          childs: []
        }
      ]
    },
    {
      path: '/category',
      name: 'Category',
      fa: 'fa fa-th-large',
      childs: [
        {
          path: '/page',
          name: 'dashboard',
          childs: []
        },
        {
          path: '',
          name: 'dashboard',
          childs: []
        }
      ]
    },
    {
      path: '/news',
      name: 'News',
      fa: 'fa fa-th-large',
      childs: [
        {
          path: '/page',
          name: 'dashboard',
          childs: []
        },
        {
          path: '',
          name: 'dashboard',
          childs: []
        }
      ]
    },
    {
      path: '/config',
      name: 'Config',
      fa: 'fa fa-th-large',
      childs: [
        {
          path: '/page',
          name: 'dashboard',
          childs: []
        },
        {
          path: '',
          name: 'dashboard',
          childs: []
        }
      ]
    },
    {
      path: '/users',
      name: 'Users',
      fa: 'fa fa-th-large',
      childs: [
        {
          path: '/user/create',
          name: 'dashboard',
          childs: [

          ]
        },
        {
          path: '',
          name: 'dashboard',
          childs: []
        }
      ]
    }
  ]
  ngOnInit() {
  }
  ngAfterViewInit() {
    this.initJquery();

  }
  initJquery() {
    // $('#side-menu').metisMenu();
  }

  link(e, item, sub) {
    // this.classActive.active = item.path;
    this._appService.setActiveLink({
      active: item,
      sub: sub
    });
    console.log(this.classActive.active);
    // console.log(item.path + ' ' + this.classActive.active);
  }

}
