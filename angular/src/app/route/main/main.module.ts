import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { LayoutsModule } from '../../layouts/layouts.module';
import { MainComponent } from './main.component';
import { PageComponent } from '../page/page.component';
import { PageModule } from '../page/page.module';
import { SuperComponent} from '../super/super.component';
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: MainComponent,
        children: [
          {
            path: 'page',
            loadChildren: '../page/page.module#PageModule'
          },
          {
            path: '',
            loadChildren: '../super/super.module#SuperModule'
          },
          {
            path: 'user',
            loadChildren: '../user/user.module#UserModule'
          }
        ]
      }
    ]),
    LayoutsModule
  ],
  declarations: [
    MainComponent,
  ]
})
export class MainModule { }
