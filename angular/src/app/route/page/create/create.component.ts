import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
declare let CKEDITOR: any;
declare var tinymce: any;
// import * as tinymce from 'tinymce';
@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  @Input() elementId: String;
  @Output() onEditorKeyup = new EventEmitter<any>();
  public editor;
  public text = 'lorem lorem';
  ngAfterViewInit() {
    this.initTinymce('#editor1');
    // this.initTinymce2('#editor2');
    
    // CKEDITOR.replace('editor1', {
    //   filebrowserBrowseUrl: server + 'assets/plugins/responsive_filemanager/filemanager/dialog.php?type=2&editor=ckeditor&fldr=&crossdomain=1',
    //   filebrowserUploadUrl:   server + 'assets/plugins/responsive_filemanager/filemanager/dialog.php?type=2&editor=ckeditor&fldr=&crossdomain=1',
    //   filebrowserImageBrowseUrl:  server + 'assets/plugins/responsive_filemanager/filemanager/dialog.php?type=1&editor=ckeditor&fldr=&crossdomain=1'
    // });
  }
  initTinymce(selector){
    let server = 'http://localhost:8000/';
    
    tinymce.init({
      selector: selector,
      height: 500,
      plugins: [
        'advlist autolink lists link image charmap print preview hr anchor pagebreak',
        'searchreplace wordcount visualblocks visualchars code fullscreen',
        'insertdatetime media nonbreaking save table contextmenu directionality',
        'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc help'
      ],
      toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
      toolbar2: 'print preview media | forecolor backcolor emoticons | codesample help',
      image_advtab: true,
      templates: [
        { title: 'Test template 1', content: 'Test 1' },
        { title: 'Test template 2', content: 'Test 2' }
      ],
      content_css: [
        '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
        '//www.tinymce.com/css/codepen.min.css'
      ],
      filemanager_crossdomain: true,
      external_filemanager_path: server + 'assets/plugins/responsive_filemanager/filemanager/',
      external_plugins: { "filemanager": server + 'assets/plugins/responsive_filemanager/filemanager/plugin.min.js' },
      // skin_url:  '/assets/plugins/responsive-filemanager/skins/lightgray',
      setup: editor => {
        this.editor = editor;
        editor.on('keyup', () => {
          const content = editor.getContent();
          console.log(content);
          this.onEditorKeyup.emit(content);
        });
      },
    });
  }
  initTinymce2(selector){
    let server = 'http://localhost:8000/';
    
    tinymce.init({
      selector: selector,
      height: 500,
      plugins: [
        'advlist autolink lists link image charmap print preview hr anchor pagebreak',
        'searchreplace wordcount visualblocks visualchars code fullscreen',
        'insertdatetime media nonbreaking save table contextmenu directionality',
        'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc help'
      ],
      toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
      toolbar2: 'print preview media | forecolor backcolor emoticons | codesample help',
      image_advtab: true,
      templates: [
        { title: 'Test template 1', content: 'Test 1' },
        { title: 'Test template 2', content: 'Test 2' }
      ],
      content_css: [
        '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
        '//www.tinymce.com/css/codepen.min.css'
      ],
      filemanager_crossdomain: true,
      external_filemanager_path: server + 'assets/plugins/responsive_filemanager/filemanager/',
      external_plugins: { "filemanager": server + 'assets/plugins/responsive_filemanager/filemanager/plugin.min.js' },
      // skin_url:  '/assets/plugins/responsive-filemanager/skins/lightgray',
      setup: editor => {
        this.editor = editor;
        editor.on('keyup', () => {
          const content = editor.getContent();
          console.log(content);
          this.onEditorKeyup.emit(content);
        });
      },
    });
  }
  showText() {
    this.text = CKEDITOR.instances.editor1.getData();
    console.log(CKEDITOR.instances.editor1.getData());
  }
}
