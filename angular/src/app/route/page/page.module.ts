import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { PageComponent } from './page.component';
import { FormsModule } from '@angular/forms';
import { LayoutsModule } from '../../layouts/layouts.module';
import { CreateComponent } from './create/create.component';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    LayoutsModule,
    RouterModule.forChild([
      {
        path: '',
        component: PageComponent
      },
      {
        path: 'create',
        component: CreateComponent
      }
    ])
  ],
  declarations: [
    PageComponent,
    CreateComponent
  ]
})
export class PageModule { }
