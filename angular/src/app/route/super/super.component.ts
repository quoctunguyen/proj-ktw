import { Component, OnInit } from '@angular/core';
import * as _ from 'lodash';
declare let fabric: any;
declare let $: any;
declare let image: any;
@Component({
  selector: 'app-super',
  templateUrl: './super.component.html',
  styleUrls: ['./super.component.css']
})
export class SuperComponent implements OnInit {
  canvas;
  itemsCir = [];

  constructor() {


  }

  ngOnInit() {

  }
  last_position: any = {};
  gianhangs = ["Bánh", "Sữa", "Mỹ phẩm", "Rau củ", "Gia dụng", "Thực phẩm"];
  position = 0;
  ngAfterViewInit() {
    this.canvas = new fabric.Canvas('canvas');
    this.canvas.width = $('#parent').width();
    this.canvas.setDimensions({
      width: $('#parent').width(),
      height: 500
    });
    var circle = new fabric.Circle({
      radius: 20, fill: 'green', left: 100, top: 100
    });
    var triangle = new fabric.Triangle({
      width: 20, height: 30, fill: 'blue', left: 50, top: 50
    });

    this.canvas.add(circle, triangle);
    this.generateGroup();
  }
  gettitle() {
    console.log('object');
  }
  generateGroup() {
    this.itemsCir.push('hi');
    console.log(this.itemsCir);
    let ar = this.getItems(this.position);
    console.log('dfsdf');
    _.times(2, (v) => {
      _.times(3, (value) => {
        // itemsCir.push(text);
        var group = new fabric.Group(ar, {
          left: 550 * v,
          top: 200 * value,
          subTargetCheck: true,
          selectable: false,
          // lockScalingX: true,
          // lockScalingY: true
        });

        if (v == 1 && value == 1) {
          // var angle = 10 % 360;
          // group.rotate(angle);
        }

        this.canvas.add(group);
        this.position++;
      })

    })
  }
  getItems(title) {
    _.times(3, (v) => {
      _.times(10, (value) => {

        var circle1 = new fabric.Rect({
          id: 'IDOF' + (value * v + v),
          top: 37 * v,
          left: 51 * value,
          width: 50,
          height: 36,
          // radius: 50,
          fill: _.sample(['#C2185B', '#1976D2']),
        });
        var name = new fabric.Text('IDOF' + (value * v + v), {
          top: 37 * v,
          left: 51 * value,
          shadow: 'rgba(0,0,0,0.2) 0 0 5px',
          fontFamily: 'Calibri',
          fill: '#003333',
          fontSize: 10
        });
        this.itemsCir.push(name);
        this.itemsCir.push(circle1);
      })

    });
    var text = new fabric.Text('Gian hàng ' + this.gianhangs[title], {
      top: -25,
      left: 0,
      shadow: 'rgba(0,0,0,0.2) 0 0 5px',
      fontFamily: 'Calibri',
      fill: '#D81B60',
      fontSize: 24
    });
    console.log('object');
    return this.itemsCir.push(text);
  }
}
