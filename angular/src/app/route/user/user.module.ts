import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { IndexComponent } from './index/index.component';
import { CreateComponent } from './create/create.component';
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: IndexComponent
      },
       {
        path: 'create',
        component: CreateComponent
      }
    ]) 
  ],
  declarations: [
    IndexComponent,
    CreateComponent
  ]
}) 
export class UserModule { }
  