import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TranslateModule } from '@ngx-translate/core';
import { ModalComponent } from './modal/modal.component';
import { BreadcrumComponent } from './breadcrum/breadcrum.component';
@NgModule({
  imports: [
    CommonModule,
    TranslateModule
  ],
  declarations: [
    ModalComponent,
    BreadcrumComponent
  ],
  exports: [
    TranslateModule,
    ModalComponent,
    BreadcrumComponent
  ]
})
export class SharedModule { }
